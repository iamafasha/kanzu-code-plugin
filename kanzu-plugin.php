<?php

/**

 * Plugin Name: Todo List Plugin
 *  Text Domain: afasha-kanzu-theme

 * Plugin URI: http://www.mywebsite.com/my-first-plugin

 * Description: The very first plugin that I have ever created.

 * Version: 1.0

 * Author: Afasha

 * Author URI: http://www.iamafasha.com
 * 
 * 

 */


 // Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

function l_loader_plugin_dir_path(){
  return plugin_dir_path(__FILE__);
}
function l_loader_plugin_dir_url(){
  return plugin_dir_url( __FILE__ );
}

require_once plugin_dir_path( __FILE__ ).'/inc/todo-custom-post-type.php';