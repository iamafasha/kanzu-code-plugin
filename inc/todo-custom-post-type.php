<?php


function create_todo_post_type() {
    $labels = array(
        'name'               => __( 'To-Do', 'afashachildtheme2021' ),
        'singular_name'      => __( 'To-Do', 'afashachildtheme2021' ),
        'menu_name'          => __( 'To-Do', 'afashachildtheme2021' ),
        'name_admin_bar'     => __( 'To-Do', 'afashachildtheme2021' ),
        'add_new'            => __( 'Add New', 'afashachildtheme2021' ),
        'add_new_item'       => __( 'Add New To-Do Item', 'afashachildtheme2021' ),
        'new_item'           => __( 'New To-Do Item', 'afashachildtheme2021' ),
        'edit_item'          => __( 'Edit To-Do Item', 'afashachildtheme2021' ),
        'view_item'          => __( 'View To-Do Item', 'afashachildtheme2021' ),
        'all_items'          => __( 'All To-Do Items', 'afashachildtheme2021' ),
        'search_items'       => __( 'Search To-Do Items', 'afashachildtheme2021' ),
        'parent_item_colon'  => __( 'Parent To-Do Items:', 'afashachildtheme2021' ),
        'not_found'          => __( 'No to-do items found.', 'afashachildtheme2021' ),
        'not_found_in_trash' => __( 'No to-do items found in Trash.', 'afashachildtheme2021' )
    );
 
    $args = array(
        'labels'             => $labels,
        'description'        => __( 'To-Do list items.', 'afashachildtheme2021' ),
        'public'             => true,
        'menu_position'      => 5,
        'supports'           => array( 'title', 'editor', 'comments', 'custom-fields' ),
        'has_archive'        => true,
        'rewrite'            => array( 'slug' => 'to-do' )
    );
 
    register_post_type( 'to_do', $args );
 }
 
 add_action( 'init', 'create_todo_post_type' );
 